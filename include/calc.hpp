/* SPDX-License-Identifier: BSD-3-Clause */
/* Copyright © 2021 Mauro Bianco */

#pragma once

#include <unordered_map>
#include <string>
#include <stdexcept>

namespace calc {
    struct evaluation_error: public std::runtime_error {
        const int position;
        public:
        evaluation_error(std::string s, int pos)
        : std::runtime_error{s}
        , position(pos)
        {}
    };

    struct parsing_error: public std::runtime_error {
        const int position;
        public:
        parsing_error(std::string s, int pos)
        : std::runtime_error{s}
        , position(pos)
        {}
    };

    double run(std::string);
    double run(std::string, std::unordered_map<std::string, double> const&);
    double run(std::string, std::unordered_map<std::string, double> const&, std::unordered_map<std::string, double(*)(double)> const&);
    double run(std::string, std::unordered_map<std::string, double> const&, std::unordered_map<std::string, double(*)(double)> &&);
    double run(std::string, std::unordered_map<std::string, double> &&, std::unordered_map<std::string, double(*)(double)> const&);
    double run(std::string, std::unordered_map<std::string, double> &&, std::unordered_map<std::string, double(*)(double)> &&);    double run(std::string, std::unordered_map<std::string, double> &&, std::unordered_map<std::string, double(*)(double)> &&);
    double run(std::string, std::unordered_map<std::string, double(*)(double)> const&);
    double run(std::string, std::unordered_map<std::string, double(*)(double)> &&);
}
