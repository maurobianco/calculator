/* SPDX-License-Identifier: BSD-3-Clause */
/* Copyright © 2021 Mauro Bianco */

#pragma once

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/ast/variant.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

#include <iostream>
#include <string>
#include <list>
#include <numeric>
#include <cmath>
#include <unordered_map>

namespace x3 = boost::spirit::x3;

namespace calc { namespace ast
{
    struct nil {};
    struct signed_;
    struct program;
    struct function_call;
    struct power;

    struct operand : x3::variant<
            nil,
            double,
            x3::forward_ast<signed_>,
            x3::forward_ast<program>,
            x3::forward_ast<function_call>,
            x3::forward_ast<power>,
            std::string
        >
    {
        using base_type::base_type;
        using base_type::operator=;
    };

    struct signed_ {
        char sign;
        operand operand_;
    };

    struct function_call {
        std::string name;
        operand argument_;
    };

    struct power {
        operand base;
        operand exponent;
    };

    struct operation {
        char operator_;
        operand operand_;
    };

    struct program {
        operand first;
        std::list<operation> rest;
    };
}}

BOOST_FUSION_ADAPT_STRUCT(calc::ast::signed_,
    sign, operand_
)

BOOST_FUSION_ADAPT_STRUCT(calc::ast::operation,
    operator_, operand_
)

BOOST_FUSION_ADAPT_STRUCT(calc::ast::function_call,
    name, argument_
)

BOOST_FUSION_ADAPT_STRUCT(calc::ast::program,
    first, rest
)

BOOST_FUSION_ADAPT_STRUCT(calc::ast::power,
    base, exponent
)

namespace calc { namespace ast
{
    struct printer
    {
        typedef void result_type;

        void operator()(nil) const {}
        void operator()(unsigned int n) const { std::cout << n; }

        void operator()(operation const& x) const {
            boost::apply_visitor(*this, x.operand_);
            switch (x.operator_)
            {
                case '+': std::cout << " add"; break;
                case '-': std::cout << " subt"; break;
                case '*': std::cout << " mult"; break;
                case '/': std::cout << " div"; break;
            }
        }

        void operator()(function_call const& f) const {
            boost::apply_visitor(*this, f.argument_);
            std::cout << f.name;
        }

        void operator()(power const& p) const {
            boost::apply_visitor(*this, p.base);
            boost::apply_visitor(*this, p.exponent);
            std::cout << "power";
        }

        void operator()(std::string const& s) const {
            std::cout << s;
        }

        void operator()(signed_ const& x) const {
            boost::apply_visitor(*this, x.operand_);
            switch (x.sign)
            {
                case '-': std::cout << " neg"; break;
                case '+': std::cout << " pos"; break;
            }
        }

        void operator()(program const& x) const {
            boost::apply_visitor(*this, x.first);
            for (operation const& oper: x.rest)
            {
                std::cout << ' ';
                (*this)(oper);
            }
        }
    };

}

    std::unordered_map<std::string, double(*)(double)> default_builtins =  []() {
            std::unordered_map<std::string, double(*)(double)> builtins;
            builtins["sin"] = std::sin;
            builtins["cos"] = std::cos;
            builtins["tan"] = std::tan;
            builtins["log"] = std::log;
            builtins["sqrt"] = std::sqrt;
            return builtins;
        }();

namespace ast {
    ///////////////////////////////////////////////////////////////////////////
    //  The AST evaluator
    ///////////////////////////////////////////////////////////////////////////
    struct eval
    {
    private:
        std::unordered_map<std::string, double> symbols;
        std::unordered_map<std::string, double(*)(double)> builtins = default_builtins;

    public:
        eval(std::unordered_map<std::string, double> const& vars)
        : symbols{vars}
        { } 

        template <typename Map>
        eval(Map && vars)
        : symbols{std::forward<Map>(vars)}
        { } 

        template <typename Map1, typename Map2>
        eval(Map1 && vars, Map2 && bins)
        : symbols{std::forward<Map1>(vars)}
        , builtins{std::forward<Map2>(bins)}
        { } 

        eval() = default;

        double operator()(nil) const { assert(false); return 0; }
        double operator()(double n) const { return n; }

        double operator()(double lhs, operation const& x) const {
            double rhs = boost::apply_visitor(*this, x.operand_);
            switch (x.operator_)
            {
                case '+': return lhs + rhs;
                case '-': return lhs - rhs;
                case '*': return lhs * rhs;
                case '/': return lhs / rhs;
            }
            throw(std::runtime_error("Operator " + std::to_string(x.operator_) + " not defined"));
            return 0;
        }

        double operator()(signed_ const& x) const {
            double rhs = boost::apply_visitor(*this, x.operand_);
            switch (x.sign)
            {
                case '-': return -rhs;
                case '+': return +rhs;
            }
            throw(std::runtime_error("Sign operator " + std::to_string(x.sign) + " not defined"));
            return 0;
        }

        double operator()(function_call const& f) const {
            double rhs = boost::apply_visitor(*this, f.argument_);
            if (builtins.find(f.name) == builtins.end()) {
                throw(std::runtime_error("Builtin function " + f.name + " not found"));
            }
            return builtins.at(f.name)(rhs);
        }
        
        double operator()(power const& p) const {
            double base = boost::apply_visitor(*this, p.base);
            double exponent = boost::apply_visitor(*this, p.exponent);
            return pow(base, exponent);
        }

        double operator()(std::string const& s) const {
            if (symbols.find(s) == symbols.end()) {
                throw(std::runtime_error("Symbol " + s + " not found in symbol table"));
            }
            return symbols.at(s);
        }

        double operator()(program const& x) const {
            return std::accumulate(
                x.rest.begin(), x.rest.end()
              , boost::apply_visitor(*this, x.first)
              , *this);
        }
    };

}}

namespace calc
{
    ///////////////////////////////////////////////////////////////////////////////
    //  The calculator grammar
    ///////////////////////////////////////////////////////////////////////////////
    namespace calculator_grammar
    {
        x3::rule<class expression, ast::program> const expression("expression");
        x3::rule<class term, ast::program> const term("term");
        x3::rule<class factor, ast::operand> const factor("factor");
        x3::rule<class name, std::string> const name("name");
        x3::rule<class builtin, ast::function_call> const builtin("builtin");
        x3::rule<class builtin, ast::power> const power("power");

        auto const expression_def =
                term
                >> *(   (x3::char_('+') >> term)
                    |   (x3::char_('-') >> term)
                    )
                ;

        auto const term_def =
                factor
                >> *(   (x3::char_('*') >> factor)
                    |   (x3::char_('/') >> factor)
                    )
                ;

        auto const factor_def =
                    x3::double_
                |   '(' >> expression >> ')'
                |   (x3::char_('-') >> factor)
                |   (x3::char_('+') >> factor)
                |   power
                |   builtin
                |   name
                ;

        auto const power_def = (x3::lit("pow") >> "(" >> expression >> "," >> expression >> ")");
                            //| (factor >> "^" >> factor); This segfaults

        auto const name_def = x3::alpha >> *x3::alpha;
        auto const builtin_def = name >> "(" >> expression >> ")";

        BOOST_SPIRIT_DEFINE(expression, term, factor, name, builtin, power);
        
        auto calculator = expression;
    }

    using calculator_grammar::calculator;

    typedef calc::ast::program program;
    typedef calc::ast::printer print;
    typedef calc::ast::eval eval;

}

