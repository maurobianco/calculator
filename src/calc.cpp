/* SPDX-License-Identifier: BSD-3-Clause */
/* Copyright © 2021 Mauro Bianco */

#include <exception>
#include "calc.hpp"
#include "expression.hpp"

namespace calc
{
    double run_eval(calc::eval const& eval, std::string const& str)
    {
        typedef std::string::const_iterator iterator_type;
        auto &calc = calc::calculator;
        calc::program program;
        calc::print print;

        iterator_type iter = str.begin();
        iterator_type end = str.end();
        boost::spirit::x3::ascii::space_type space;
        bool r = phrase_parse(iter, end, calc, space, program);

        if (r && iter == end)
        {
            try {
            return eval(program);
            } catch (std::runtime_error const& e) {
                throw(evaluation_error(e.what() + std::string(" in position [not available] ") + std::to_string(iter - str.begin()), iter - str.begin()));
            }
        }
        else
            throw(parsing_error("Parse error in expression in position " + std::to_string(iter-str.begin()), iter-str.begin()));
    }

    double run(std::string const& str) {
        calc::eval eval;
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> const& symbols)
    {
        calc::eval eval{symbols};
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> &&symbols)
    {
        calc::eval eval{std::move(symbols)};
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> const& symbols, 
                std::unordered_map<std::string, double(*)(double)> const& bltins)
    {
        calc::eval eval{symbols, bltins};
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> &&symbols, 
                std::unordered_map<std::string, double(*)(double)> const& bltins)
    {
        calc::eval eval{std::move(symbols), bltins};
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> const& symbols, 
                std::unordered_map<std::string, double(*)(double)> && bltins)
    {
        calc::eval eval{symbols, std::move(bltins)};
        return run_eval(eval, str);
    }

    double run(std::string str, std::unordered_map<std::string, double> &&symbols, 
                std::unordered_map<std::string, double(*)(double)> && bltins)
    {
        calc::eval eval{std::move(symbols), std::move(bltins)};
        return run_eval(eval, str);
    }
}