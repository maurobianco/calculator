/* SPDX-License-Identifier: BSD-3-Clause */
/* Copyright © 2021 Mauro Bianco */

#include <unordered_map>
#include <calc.hpp>
#include <iostream>
#include <gtest/gtest.h>
#include <cmath>

struct calc_tests : public testing::Test
{
    typedef std::string::const_iterator iterator_type;

    std::vector<std::pair<std::string, double>> tests{
        {"0.12 + 0.03", 0.15},
        {"12 + 3", 15},
        {"sin ( 3 )", sin(3)},
        {"cos(3)", cos(3)},
        {"sin(3)+cos(3)", sin(3) + cos(3)},
        {"x", 10},
        {"x*2+10", 30},
        {"pow(x, (x * 3 - 10)/10)+10", 110},
        {"sin(x)", sin(10)},
        {"sqrt(16)", sqrt(16)},
        {"1+(", -1},
        {"sin(30", -1},
        {"atan(4)", -2},
        {"sin(xyz)", -2}};
};

TEST(calc, lib)
{
    {
        double res = calc::run("x+10", std::unordered_map<std::string, double>{{"x", 10}});
        EXPECT_EQ(res, 20);
        //std::cout << ((res == 20) ? "Passed " : "Failed ") << 20 << " = " << res << " (" << res << ")\n";
    }
}

TEST(calc, lib1)
{
    std::unordered_map<std::string, double (*)(double)> builtins;
    builtins["sin"] = std::sin;
    builtins["cos"] = std::cos;
    builtins["tan"] = std::tan;
    builtins["log"] = std::log;
    builtins["sqrt"] = std::sqrt;
    double res = calc::run("sqrt(x)+10", std::unordered_map<std::string, double>{{"x", 36}}, builtins);
    EXPECT_EQ(res, 16);
    //std::cout << ((res == 16) ? "Passed " : "Failed ") << 16 << " = " << res << " (" << res << ")\n";
}

TEST_F(calc_tests, several)
{
    for (auto &pair : tests)
    {
        std::unordered_map<std::string, double (*)(double)> builtins;
        builtins["sin"] = std::sin;
        builtins["cos"] = std::cos;
        builtins["tan"] = std::tan;
        builtins["log"] = std::log;
        builtins["sqrt"] = std::sqrt;

        if (pair.second == -1)
        {
            EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, builtins), std::runtime_error);
        }
        else if (pair.second == -2)
        {
            EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, builtins), std::runtime_error);
        }
        else
            EXPECT_EQ(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, builtins), pair.second);
    }
}

TEST_F(calc_tests, several_move)
{
    std::unordered_map<std::string, double (*)(double)> builtins_;
    builtins_["sin"] = std::sin;
    builtins_["cos"] = std::cos;
    builtins_["tan"] = std::tan;
    builtins_["log"] = std::log;
    builtins_["sqrt"] = std::sqrt;

    for (auto &pair : tests)
    {
        if (pair.second == -1)
        {
            {
                auto builtins = builtins_;
                EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins)), std::runtime_error);
            }
            {
                auto builtins = builtins_;
                EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins)), calc::parsing_error);
            }
            {
                auto builtins = builtins_;
                try {
                calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins));
                } catch (calc::parsing_error const&e) {
                    std::cout << e.what() << " -> " << e.position << "\n";
                }
            }
        }
        else if (pair.second == -2)
        {
            {
                auto builtins = builtins_;
                EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins)), std::runtime_error);
            }
            {
                auto builtins = builtins_;
                EXPECT_THROW(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins)), calc::evaluation_error);
            }
            {
                auto builtins = builtins_;
                try {
                calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins));
                } catch (calc::evaluation_error const&e) {
                    std::cout << e.what() << " -> " << e.position << "\n";
                }
            }
        }
        else
        {
            auto builtins = builtins_;
            EXPECT_EQ(calc::run(pair.first, std::unordered_map<std::string, double>{{"x", 10}}, std::move(builtins)), pair.second);
        }
    }
}
