/* SPDX-License-Identifier: BSD-3-Clause */
/* Copyright © 2021 Mauro Bianco */

#include <unordered_map>
#include <gtest/gtest.h>
#include "expression.hpp"

struct calc_tests : public testing::Test
{
    typedef std::string::const_iterator iterator_type;

    std::vector<std::pair<std::string, double>> tests{
        {"0.12 + 0.03", 0.15},
        {"12 + 3", 15},
        {"sin ( 3 )", sin(3)},
        {"cos(3)", cos(3)},
        {"sin(3)+cos(3)", sin(3) + cos(3)},
        {"x", 10},
        {"x*2+10", 30},
        {"pow(x, (x * 3 - 10)/10)+10", 110},
        {"sin(x)", sin(10)},
        {"sqrt(16)", sqrt(16)},
        {"1+(", -1},
        {"sin(30", -1},
        {"atan(4)", -2},
        {"sin(xyz)", -2}};

    template <typename Pair>
    void test(Pair pair, calc::eval const &eval)
    {
        auto &calc = calc::calculator;
        calc::program program;
        calc::print print;

        iterator_type iter = pair.first.begin();
        iterator_type end = pair.first.end();
        boost::spirit::x3::ascii::space_type space;
        bool r = phrase_parse(iter, end, calc, space, program);

        if (pair.second == -1)
        { // expecting failure
            //std::cout << pair.first << "\n";
            EXPECT_FALSE(r && iter == end);
        }
        else if (r && iter == end)
        {
            if (pair.second != -2)
            {
                double res = eval(program);
                EXPECT_EQ(res, pair.second);
                //std::cout << ((res == pair.second) ? "Passed " : "Failed ") << pair.first << " = " << res << " (" << pair.second << ")\n";
            }
            else
            {
                EXPECT_THROW(eval(program), std::runtime_error);
            }
        }
    }
};

TEST_F(calc_tests, test_constructors1)
{
    for (auto &pair : tests)
    {
        std::unordered_map<std::string, double> symbols{{"x", 10}};
        calc::eval eval(symbols);
        test(pair, eval);
    }
}

TEST_F(calc_tests, test_constructors2)
{
    for (auto &pair : tests)
    {
        std::unordered_map<std::string, double> symbols{{"x", 10}};
        calc::eval eval(std::move(symbols));
        test(pair, eval);
    }
}

TEST_F(calc_tests, test_constructors3)
{
    for (auto &pair : tests)
    {
        std::unordered_map<std::string, double> symbols{{"x", 10}};
        calc::eval eval(symbols, calc::default_builtins);
        test(pair, eval);
    }
}

TEST_F(calc_tests, test_constructors4)
{
    for (auto &pair : tests)
    {
        std::unordered_map<std::string, double> symbols{{"x", 10}};
        auto bltins = calc::default_builtins;
        calc::eval eval(std::move(symbols), std::move(bltins));
        test(pair, eval);
    }
}
